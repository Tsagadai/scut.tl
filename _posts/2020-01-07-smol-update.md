---
layout: post
title: No updates yesterday?
---

Too many private repos. There were commits, they just weren't in the open.
I am going to endeavour to stick to one public commit a day so hopefully
that lapse just slides. 

Today I found a fun and profitable bug in AWS, a cheap and easy way to get 
free compute. I'll escalate it back to AWS soon as I get around to writing
it up though.
