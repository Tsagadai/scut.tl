---
layout: post
title: Moar posts for the post gods
---

Lockdowns and isolation are still playing with my ability to get things done. It is amazing how much we, as humans, need socialisation
to stay frosty and fight to be better. Gets to me eventually too even as a hermit most of the time. I am slowly making progress on various projects though but I am becoming increasingly aware that I need to focus on delivery so I don't end up with a massive pile of half completed projects. Always a challenge so I am starting small and focusing on spending at least one hour per day on unique creation and being kinder to myself for not getting anything done. It is counterintuitive but you need to start by doing and then narrow in on doing well. The hardest step is always the first.
