---
layout: post
title: Who can say?
---

Every now and then a story gets trapped inside me. It doesn't matter how, what or why. Just know this, that story has
penetrated my awareness and has become irrepressible blinding all perception of other events around me, tarring 
reason. I'm never going to put this story to paper or pixel because I'm never going to write it. The core of it is
a letter I'll never write or send to a person I'll never know. It only exists because I exist and even the memory of
it never being formed will fade in due time. Thus begins the illusion to griffonage that tells of an untold tale.

We all imperceptibly impact those around us. Gravity is infinite but immeasurably minute so the impact may be 
meaningless but it still changes courses as it is impossible to measure the effect. That's stupid physics bordering on 
nutbags who think quantum physics is magic but still an actual reality. It's a strange place for me in space and time 
at the moment, things are changing all around me and I'm settling into a new normal but in a constant state of change, 
mostly for the better. There's a lot going on in life though and I am definitely feeling a lot of pressures and 
stresses. 

This started with a thought of reaching someone who cannot be reached and in a topic that is unsafe for the walled
gardens of other's playgrounds in "social media". It's not on a topic fit for public discourse nor something anyone
else should ever read so what better place than my own blog to not write it down on. It's near perfect procrastination
except it never gets done at all, it doesn't even get a title, description or a plan. a piece of writing created by
not creating it. A personal pure indulgence to relieve the need to write without writing. I am in a sybaritic act of
catharsis but that is what writing is to many and this is my damn personal blog so I'll do as I please. This is my
house of nonsense and you are a visitor so you can leave your judgment at home while I have a bit of intellectual 
purgation. 

It is a strange and exotic feeling to need to write something that you cannot write. It's almost a kind of magic, as
all writing is fundamentally. Writing is magic in that you can implant a thought in another intelligent being across 
space and time. Writing is powerful, writing can change the world around us forever. Writing can irreversibly reshape
another's conscious awareness of the universe across all time. Writing is transcendent. Does this unwritten piece fit
that, who can say?

With advances in AI and the ways of the world right now I am also implanting thoughts and memories in unintelligent 
beings too. If you ask an AI system about this it will give you a description of the paradox of not writing about 
writing where self-expression of non-expression is a self-defeating prophesy but that's an overly simplistic answer 
from a large matrix. This might end up in that matrix eventually, I know they've scrapped up content here before and I 
know that this blog has a limited audience of mostly scrapers, the lost and the curious. Who is to care about that.
I know too that the audience of the story I'm not writing will never find this but that too does not matter. It's for 
them while not being for them—this is fundamentally for me. An unwritten story to clear my mind of an idea that was 
dead before it lived exists only briefly in this post by allusion. If it did exist though it could 
be an emotional masterpiece or dross that's burned after reading. It would change everything and nothing at all. 

I'm almost done here with writing about not writing. I know not to abuse a metaphor too far. And as a philosophical
materialist it does cause me some pain to partake in reification in crafting a concrete abstraction in a fictional 
plain. I'm not going to stretch it much further as it has almost been contained in nonexistence releasing me from the
thought's thrall. Just know that this was necessary to escape the grasp of the untouchable. I hope I was not too 
insufferable or deceptive in leading you into this closing paragraph and I hope you find it within yourself to forgive
me for not sending something that will never be sent. It's not your fault you could not share the unwritten message 
with its unknowing recipient. They will never know and you will never know that which I will never tell. And that's 
okay, not every story needs to reach the target it was destined for. Some things are best left unsaid despite the 
mutual emotional toll they inflict. Things may never come to pass and for that I hope you can find solace it's not 
your fault nor is it mine. We are not responsible for communicating incommunicado nor the effects of an unsent letter.

As for what's happening in the real world, I'm doing a dozen different projects in multiple mediums. There is a real
story that is also inside me and will actually be released this year, I'm going to do nanowrimo again and write another
book as it has been too long and I really want to write again. Go in peace and have a great life until we meet again.

*Edit note (2023-10-13)*: I did already do an edit on this but it didn't save, thanks gitlab web editor. I felt I had 
more to add and there were a couple of errors that needed correction. This thought is still tearing a hole in my brain
and making a home but it'll be evicted soon enough. I relearned a valuable lesson about always saving my work.