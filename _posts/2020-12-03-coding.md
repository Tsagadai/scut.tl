---
layout: post
title: Coding
---

I forgot how fun leetcode style problems can be when you are just solving them for fun and not interviews
or punishing interview training. I am doing the [Advent of Code 2020](https://adventofcode.com/2020) and I am on 100% so far and still enjoying it.
