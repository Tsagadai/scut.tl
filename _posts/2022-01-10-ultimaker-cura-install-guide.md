---
layout: post
title: Ultimaker Cura Linux Install Guide
---

Ever Google something and get only stupid answers, yeah me too. Here is a very short guide to running Ultimaker Cura on Linux. The packaged version of Cura, while available on Ubuntu is pretty ancient so not really worth it. `apt get cura` works but the version is 2 years old so this guide is if you want to use the latest binary.

### Step 1: Download

[Download the software from Ultimaker](https://ultimaker.com/software/ultimaker-cura)

Remember where you downloaded it because you need that for step two.

### Step 2: Change the permissions

Change the permissions on the AppImage file. Open a terminal and navigate to the directory you downloaded the file to. Then run the below command modifying it to use the version you downloaded (I downloaded 4.12.1)

```
$ chmod +x Ultimaker_Cura-4.12.1.AppImage
```

### Step 3: Run Cura

From the same terminal:

```
$ ./Ultimaker_Cura-4.12.1.AppImage
```

### Optional Step 4: Add a shortcut to Ubuntu Gnome

Something I did which may be useful to you if you are using Gnome or base Ubuntu.

Create a file for the application shortcut by opening the file `/usr/share/applications/cura.desktop` as root and insert the following. If you want to do it without root open `~/.local/share/applications/cura.desktop` instead.

```
[Desktop Entry]
Name=Ultimaker Cura
GenericName=3D Printing Software
Comment=Cura converts 3D models into paths for a 3D printer. It prepares your print for maximum accuracy, minimum printing time and good reliability with many extra features that make your print come out great.
Exec=/opt/Ultimaker_Cura-4.12.1.AppImage %F
TryExec=/opt/Ultimaker_Cura-4.12.1.AppImage
Icon=cura-icon
Terminal=false
Type=Application
MimeType=model/stl;application/vnd.ms-3mfdocument;application/prs.wavefront-obj;image/bmp;image/gif;image/jpeg;image/png;text/x-gcode;application/x-amf;application/x-ply;application/x-ctm;model/vnd.collada+xml;model/gltf-binary;model/gltf+json;model/vnd.collada+xml+zip;
Categories=Graphics;Education;Development;Science;
Keywords=3D;Printing;Slicer;
StartupWMClass=cura.real
```

Write the replacing `/opt/Ultimaker_Cura-4.12.1.AppImage` with wherever your file is, `/opt` isn't a bad place to put it. 

Run `desktop-file-validate` to make sure everything is happy.

```
$ desktop-file-validate /usr/share/applications/cura.desktop
```

Now everything should be set up and you should be able to access Cura from the menu and have filetype associations in Gnome file managers.
