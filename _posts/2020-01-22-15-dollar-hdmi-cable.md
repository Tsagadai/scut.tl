---
layout: post
title: $15 HDMI cable
---

I often take alternative approaches to getting things working. Things break and
I am a tinkerer by nature, or things don't work as originally intended. I'm 
still on leave so spenind more time out and about than committing code but at
the end of the day I still want to consume some media sometimes (that said, I
am trying to consume less and produce more). Chromecasts seem to fight with
hotel wifi networks that require silly unencrypted sign-in screens. This is a 
pain but not impossible to workaround with tethering and multiple networks and
whatnot. That is what lead me to buying a $15 HDMI 1.5m HDMI cable. What a 
complete waste of money.