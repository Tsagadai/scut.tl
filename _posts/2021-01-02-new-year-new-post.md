---
layout: post
title: New Year, New Post
---

Fell off the posting wagon in December. It is very hard to stay on it with festivities, family and freedom. I have been busy but I also injured myself over a week ago so I have also been mostly prone or in intense pain which also hampers my ability to focus on anything. My back is mostly better now

Today, I've managed to get my NAS cleaned up and remade into a more useful format. I now have three (yes, three) shared storage machines. The one I fixed today is very highly optimized and it is using the finest parts so we have a 20TB ZFS (made of 5TB disks in mirrored vdevs) array backed by an SSD cache and a mirrored special drive of two SSDs. There is also a separate fast NVMe RAID running on ext4 for dedicated fast storage. Also upgraded the network there to 10GBe to the main machines it works with. The other two file servers are 20TB and 60TB usable but made up of older drives and very large ZFS arrays. The main "NAS" is also running an old Ryzen 1800 and it has an Nvidia 1080 so it can join my expanding GPU cluster as needed (I acquired a bunch of Pascal era Quadro cards too which I'm going to add into the mix soon). I am still pushing my homelabbing into more AI and applications. I want to be efficiently harnessing a lot of compute for some quite varied tasks over the coming months and I am learning a lot more about using devops to get what I want out of hardware instead of just good design (but learning a lot about the right way along the way). My personal needs are quite different from corporate needs. I don't really have a high-availability requirement and I am often messing around with older hardware so I am far more focused on ease of deployment, failover and power management.

Anyway, back to it.
