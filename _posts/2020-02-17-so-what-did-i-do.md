---
layout: post
title: So what did I do?
---

Today I did a very basic app in Go with [walk](https://github.com/lxn/walk).
I have never done much in Go before other than toy projects so another toy
project was just what I needed. I picked go because it had to be a Windows 
function and writing it in just a scripting language (like DOS or powershell)
while possible would have been painful and writing it in python, or whatnot 
would have needed too much support. I've built cross platform windows tools in
python before, it is days of messing around to get everything fitting together.

Go was easy, it did the job and with google and the docs I could have a 
reasonably tricky script done in a few hours.

The other thing I did was try to flash my phone. I didn't get too far on that 
because I had to unlock the bootloader which is a multistage process but I'll 
have it done in a week. I am blocked there by the phone's desire to phone home
to the mothership and ask for permission. Now you know why that crap needs 
flashing...