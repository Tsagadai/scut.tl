---
layout: post
title: What in the what
---

I've been quiet. I'm sure I am not the only one. Big Rona has been lurking around ruining
economies and the like. I've still got a job though. Even managed to find some coins to 
repair my abode and put power generation on it. Other than that I have been pretty quiet.
Still working, and being busy. I don't really know where my time is going at the moment,
I am getting a lot done but not getting a lot done at the same time. Paradoxical 
productivity.

For now it is just a short howdy do. I am planning to do things with another site I own
very soon and making that into a kind of side hustle.

Edit: I probably should add that I have been mostly sticking to my goals. I have had 7
tasks that I aim to do at least something on each day. Generally, I am hitting 4-5 each
day but I'll have a post with some stats soon.