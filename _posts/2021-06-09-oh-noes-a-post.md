---
layout: post
title: Oh noes, a post
---

I think I just tried to commit a post from another account, my bad. I may have even forked my own blog. The confusing thing of having too many version control systems.

Anyways, I've been making and creating. All good and wholesome stuff. Lots of data projects on the go and I might even have some longer posts soon on a few topics. I am trying to hold myself accountable to actually finish things lately and it seems to work in actually increasing my output for the time being. Winter is coming.
