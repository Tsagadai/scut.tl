---
layout: post
title: Sit down and listen to a story of my ancestors
---

I started fixing things tonight on my web assets. 

Fixed a ton of broken links of my own creation and added some content to a few different pages. 

Discovered that CSS from a decade ago sucks really, really bad on modern browsers.

And I put something on the front page of <scut.tl>. I even learnt that markdown has expanded slightly and broken the original purpose, of being a simple markup language. 

The more you know.
