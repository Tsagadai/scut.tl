---
layout: page
title:  "About scut.tl"
---

This site is mostly a vanity blog for me. I intended to do many things with it
over the years but for now it is just where I write when I want to shout into
the void.