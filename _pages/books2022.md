---
layout: default
title:  "Books I've read in 2022"
---

I tend to read a bit but not always and I am pretty bad at tracking what I've read.
So instead of relying on some 3rd party that'll just sell me more copies of the 
books I've already read I thought I'd track my reading here instead on my own 
site on my own terms.

### January 
* Leviathan Falls by James S. A. Corey